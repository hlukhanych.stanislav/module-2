﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Зав2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<string> strings;
        List<SP> list;
        BindingSource BindSource;
        private void Form1_Load(object sender, EventArgs e)
        {
            strings = new List<string>();
            list = new List<SP>();
            BindSource = new BindingSource();

            list.Add(new SP("Unet", "15", "Internet", "Ukraine, Poland"));
            list.Add(new SP("Cafe White", "7", "Coffee", "Ukraine"));
            list.Add(new SP("Bank", "80", "Money", "Ukraine, USA, Germany"));
            list.Add(new SP("Toy Shop", "18", "Toy", "Ukraine"));
            list.Add(new SP("Supermarket", "33", "Products", "Slovakia"));

            BindSource.DataSource = list;
            dataGridView1.DataSource = BindSource;

            dataGridView1.Columns["Name"].HeaderText = "Назва підприємства";
            dataGridView1.Columns["Count"].HeaderText = "Кількість співробітників";
            dataGridView1.Columns["ProductName"].HeaderText = "Назва продукції";
            dataGridView1.Columns["Country"].HeaderText = "Назви країн";
        }

        private void завантажитиВТекстовийФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(saveFileDialog1.FileName);

                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    for (int j = 0; j < dataGridView1.ColumnCount; j++)
                    {
                        sw.WriteLine(dataGridView1[j, i].Value);
                    }
                }

                sw.Close();

                MessageBox.Show("Успішно");
            }
        }

        List<SP> SPFiltered;
        BindingSource BindSourceFiltered;
        private void button1_Click(object sender, EventArgs e)
        {
            string name = textBox1.Text;
            SPFiltered = list.FindAll(product => product.Country.Contains(name));

            BindSourceFiltered = new BindingSource();
            BindSourceFiltered.DataSource = SPFiltered;

            dataGridView2.DataSource = BindSourceFiltered;

            dataGridView2.Columns["Name"].HeaderText = "Назва підприємства";
            dataGridView2.Columns["Count"].HeaderText = "Кількість співробітників";
            dataGridView2.Columns["ProductName"].HeaderText = "Назва продукції";
            dataGridView2.Columns["Country"].HeaderText = "Назви країн";
        }

        private void завантажитиЗТекстовогоФайлаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                dataGridView1.DataSource = new object();
                dataGridView2.DataSource = new object();
                strings.Clear();
                list.Clear();
                BindSource.Clear();

                StreamReader sr = new StreamReader(openFileDialog1.FileName);
                while (!sr.EndOfStream)
                {
                    strings.Add(sr.ReadLine());
                }
                sr.Close();

                for (int i = 0; i < strings.Count; i++)
                {
                    list.Add(new SP(strings[i], strings[i + 1], strings[i + 2], strings[i + 3]));
                    i++;
                    i++;
                    i++;
                }

                BindSource.DataSource = list;
                dataGridView1.DataSource = BindSource;

                dataGridView1.Columns["Name"].HeaderText = "Назва підприємства";
                dataGridView1.Columns["Count"].HeaderText = "Кількість співробітників";
                dataGridView1.Columns["ProductName"].HeaderText = "Назва продукції";
                dataGridView1.Columns["Country"].HeaderText = "Назви країн";
            }
        }

        private void експортВExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Operations.saveGridToExcel(dataGridView1, saveFileDialog1.FileName, "List1");
            }
        }
    }
}
