﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Зав2
{
    internal class SP
    {
        public string Name { get; set; }
        public string Count { get; set; }
        public string ProductName { get; set; }
        public string Country { get; set; }
        public SP(string name, string count, string productName, string country)
        {
            Name = name;
            Count = count;
            ProductName = productName;
            Country = country;
        }
    }
}
