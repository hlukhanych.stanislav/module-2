﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Зав1
{
    internal class Operations
    {
        public static void oper(string fileName, DataGridView grid)
        {
            StreamReader sr = new StreamReader(fileName);

            List<int> numbers = new List<int>();
            List<int> res = new List<int>();

            while (!sr.EndOfStream)
            {
                string data = sr.ReadLine();

                char[] sep = new char[] { ' ' };

                string[] parts = data.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < parts.Length; i++)
                {
                    numbers.Add(Convert.ToInt32(parts[i]));
                }

                res.Add(numbers.Max());

                numbers.Clear();
            }

            grid.ColumnCount = 1;
            grid.RowCount = res.Count;

            grid.Columns[0].HeaderText = "Найбільші елементи масивів";

            for (int i = 0; i < res.Count; i++)
            {
                grid[0, i].Value = res[i];
            }

            sr.Close();
        }
    }
}
